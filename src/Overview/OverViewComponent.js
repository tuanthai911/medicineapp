import React from 'react';
import { numberWithCommas } from '../Helpers/MoneyHelper.js';
import { LanguageObject } from '../Helpers/LanguageHelper.js';

class OverView extends React.Component {
    render() {
        var languageDic = LanguageObject(this.props.language);
        return (
            <div className="border over-view">
                <div>
                    <div><i className="fa fa-shopping-cart"></i> {languageDic.OverViewComponent.Package}</div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col text-center border">
                            <h1>{this.props.NumberProductChoosen}</h1>
                            <small>{languageDic.OverViewComponent.Products}</small>
                        </div>
                        <div className="col text-center border">
                            <h1>{numberWithCommas(this.props.TotalPrice, this.props.language)}</h1>
                            <small>{languageDic.PriceTag}</small>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default OverView;