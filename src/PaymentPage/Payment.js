import React from 'react';
import { Link } from 'react-router-dom';
import ProductTable from '../FilterProduct/ProductTable.js';
import { numberWithCommas } from '../Helpers/MoneyHelper.js';
import { LanguageObject } from '../Helpers/LanguageHelper.js'

class PaymentPage extends React.Component {
    constructor(props) {
        super(props);
        this.handleProductsChange = this.handleProductsChange.bind(this);
    }

    handleProductsChange() {
        this.forceUpdate();
    }

    render() {
        var language = this.props.location.state.language;
        var languageDic = LanguageObject(language);
        var totalPrice = 0;
        var totalChoose = 0;
        this.props.location.state.products.forEach(
            (product) => {
                if (product.totalChoosen !== 0) {
                    totalChoose += product.totalChoosen;
                }
                totalPrice += product.totalChoosen * product.price;
            }
        );
        return (
            <div className="row" style={{ margin: "0 1%" }}>
                <div className="col-12">
                    <h4 style={{ margin: "20px 0 14px 0" }}>
                        {languageDic.PaymentPage.Hello} {this.props.location.state.name}
                        <span style={{ fontSize: "14px", padding: "0 10px 0 10px" }}> {languageDic.PaymentPage.TotalChoose.replace("{0}", totalChoose)}</span>
                        <Link to="/">
                            <button type="button" className="btn btn-info">
                                <span className="fa fa-shopping-cart"></span> {languageDic.PaymentPage.BackToStore}
                            </button>
                        </Link>
                    </h4>
                </div>
                <div className="col-8" style={{ paddingTop: "1%" }}>
                    <ProductTable
                        userInput=""
                        products={this.props.location.state.products}
                        currentPage="1"
                        handleProductsChange={this.handleProductsChange}
                        isGetall="true"
                        language={language}
                    />
                </div>
                <div class="col-4" style={{ paddingTop: "1%" }}>
                    <div style={{ backgroundColor: "gainsboro", width: "270px" }}>
                        <div class="each-row">
                            <div style={{ padding: "17px 20px 21px 19px" }}>
                                <p>
                                    <span>{languageDic.PaymentPage.FirstCal}: </span>
                                    <strong style={{ fontsize: "22px", fontWeight: "400", float: "right" }}>{numberWithCommas(totalPrice, language)}&nbsp;₫</strong>
                                </p>
                            </div>
                            <div style={{ padding: "17px 20px 21px 19px" }}>
                                <div class="clearfix">
                                    <span>{languageDic.PaymentPage.TotalPay}: </span>
                                    <div style={{ float: "right" }}>
                                        <p>
                                            <strong style={{ marginTop: "-5px", color: "#fe3834", fontSize: "22px", fontWeight: "400", float: "right" }}>{numberWithCommas(totalPrice, language)}&nbsp;₫ </strong>
                                        </p>
                                        <p style={{ float: "right" }}>
                                            <small>({languageDic.PaymentPage.IncludeVAT})</small>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                );
            }
        }
        
export default PaymentPage;