import React from 'react';
import SearchBar from './SearchBar.js';
import ProductTable from './ProductTable.js'
import Paging from './Paging.js'
import ModalBooking from './BookingModel.js'

class FilterableProductTable extends React.Component {
  constructor(props) {
    super(props);
    this.handleSearchInput = this.handleSearchInput.bind(this);
    this.handlePagingButton = this.handlePagingButton.bind(this);
    this.handleProductsChange = this.handleProductsChange.bind(this);
    this.state = {
      input: '',
      currentPaging: 1
    };
  }

  handleProductsChange() {
    var totalPrice = 0;
    var totalChoose = 0;
    this.props.products.forEach(
      (product) => {
        if (product.totalChoosen !== 0) {
          totalChoose++;
        }
        totalPrice += product.totalChoosen * product.price;
      }
    );

    this.props.handleProductItemUpdate(totalChoose, totalPrice);
  }

  handleSearchInput(userInput) {
    this.setState({
      input: userInput,
      currentPaging: 1
    }, function () { })
  }

  handlePagingButton(page) {
    this.setState({
      input: this.state.input,
      currentPaging: page
    })
  }

  render() {
    const userInput = this.state.input;
    var filterProduct = [];
    this.props.products.forEach(
      (product) => {
        if (this.props.isChooseState === true) {
          if (product.name.toLowerCase().indexOf(userInput.toLowerCase()) === -1) {
            return;
          }
        }
        else {
          if (product.totalChoosen === 0) {
            return;
          }
        }

        filterProduct.push(product);
      }
    );

    var totalPage = filterProduct.length / 5;
    if (filterProduct.length % 5 !== 0) {
      totalPage++;
    }

    var container = [];
    if (this.props.isChooseState === true) {
      container.push(
        <SearchBar
          onSearchChange={this.handleSearchInput}
          language={this.props.language}
        />
      );
    }
    else {
      container.push(
        <ModalBooking
          products={filterProduct}
          language={this.props.language}
        />
      );
    }

    container.push(
      <ProductTable
        userInput={userInput}
        products={filterProduct}
        currentPage={this.state.currentPaging}
        handleProductsChange={this.handleProductsChange}
        language={this.props.language}
      />
    );

    if (filterProduct.length !== 0) {
      container.push(
        <Paging
          totalPage={totalPage}
          currentPage={this.state.currentPaging}
          onPageClick={this.handlePagingButton}
        />
      );
    }

    return (
      <div style={{ marginTop: "5%" }}>
        {container}
      </div>
    );
  }
}

export default FilterableProductTable;