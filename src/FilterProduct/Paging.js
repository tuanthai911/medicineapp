import React from 'react';
import { PagingConst } from '../Helpers/PagingConst.js'

class Paging extends React.Component {
    constructor(props) {
        super(props);
        this.handleNumberButton = this.handleNumberButton.bind(this);
        this.handleStartButton = this.handleStartButton.bind(this);
        this.handleEndButton = this.handleEndButton.bind(this);
    }

    handleNumberButton(e) {
        this.props.onPageClick(e.target.value);
    }

    handleStartButton(e) {
        this.props.onPageClick(1);
    }

    handleEndButton(e) {
        this.props.onPageClick(parseInt(this.props.totalPage, 10));
    }

    render() {
        var currentPageNum = parseInt(this.props.currentPage, 10);
        var totalPageNum = parseInt(this.props.totalPage, 10);
        var start = 0, end = 0;
        if (currentPageNum === 1) {
            start = PagingConst.startPageNumber;
            end = 3
        }
        else if (currentPageNum === totalPageNum) {
            start = currentPageNum - 2;
            end = totalPageNum;
        }
        else {
            start = currentPageNum - 1;
            end = currentPageNum + 1;
        }
        var pagingRows = [];
        pagingRows.push(
            <button onClick={this.handleStartButton} key="begin">&laquo;</button>
        );
        if (start >= 3) {
            pagingRows.push(
                <input
                    type="button"
                    value={1}
                    onClick={this.handleNumberButton}
                    key={1}
                />
            );
            pagingRows.push(
                <input
                    type="button"
                    value={2}
                    onClick={this.handleNumberButton}
                    key={2}
                />
            );
            pagingRows.push(
                <input type="button" value='...' disabled />
            );
        }
        for (var i = start; i <= end; i++) {
            if (i === currentPageNum) {
                pagingRows.push(
                    <input
                        type="button"
                        style={{ color: 'red' }}
                        value={i}
                        onClick={this.handleNumberButton}
                        key={i}
                    />
                );
            }
            else {
                pagingRows.push(
                    <input
                        type="button"
                        value={i}
                        onClick={this.handleNumberButton}
                        key={i}
                    />
                );
            }
        }
        if (end <= totalPageNum - 2) {
            pagingRows.push(
                <input type="button" value='...' disabled />
            );
            pagingRows.push(
                <input
                    type="button"
                    value={totalPageNum - 1}
                    onClick={this.handleNumberButton}
                    key={totalPageNum - 1}
                />
            );
            pagingRows.push(
                <input
                    type="button"
                    value={totalPageNum}
                    onClick={this.handleNumberButton}
                    key={totalPageNum}
                />
            );
        }

        pagingRows.push(
            <button onClick={this.handleEndButton} key="end">&raquo;</button>
        );

        return (
            <div>{pagingRows}</div>
        );
    }
}

export default Paging;