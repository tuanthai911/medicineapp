import React from 'react';
import ProductRow from './ProductRow.js'
import { PagingConst } from '../Helpers/PagingConst.js'
import { LanguageObject } from '../Helpers/LanguageHelper.js';

class ProductTable extends React.Component {
    constructor(props){
      super(props);
      this.handleProductRowsChange = this.handleProductRowsChange.bind(this);
    }

    handleProductRowsChange(){
      this.props.handleProductsChange();
    }

    render(){
      const rows = [];
      var productRows = [];
      var languageDic = LanguageObject(this.props.language);
  
      this.props.products.forEach(
        (product) => {
          rows.push(
            <ProductRow 
              handleProductRowsChange={this.handleProductRowsChange}
              product={product}
              key={product.name}
              language={this.props.language}
            />
          )
        }
      );
      
      if(this.props.isGetall !== undefined && this.props.isGetall === "true"){
        productRows = rows;
      }
      else{
        var startIndex = (this.props.currentPage - 1) * PagingConst.numberOfItem;
        var endIndex = this.props.currentPage * PagingConst.numberOfItem;
        productRows = rows.slice(startIndex, endIndex);
      }
  
      return (
        <table className="table">
          <thead>
            <tr>
              <th>{languageDic.ProductTableFile.ProductName}</th>
              <th></th>
              <th>{languageDic.ProductTableFile.ProductPrice}</th>
              <th style={{width: "20%"}}>{languageDic.ProductTableFile.ProductTotalMoney}</th>
            </tr>
          </thead>
          <tbody>{productRows}</tbody>
        </table>
      );
    }
  }

export default ProductTable;