import React from 'react';
import { numberWithCommas } from '../Helpers/MoneyHelper.js'
import { Link } from 'react-router-dom'
import CounterRowComponent from '../CounterRowComponent/CounterRowComponent.js'
import { LanguageObject } from '../Helpers/LanguageHelper.js'

class ProductRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputNumber: 0,
            totalRowPrice: 0
        }
        this.handleCounterRowChange = this.handleCounterRowChange.bind(this);
    }

    handleCounterRowChange(inputNum){
        this.setState({
            inputNumber: inputNum,
            totalRowPrice: inputNum * this.props.product.price
        })
        this.props.handleProductRowsChange();
    }

    render() {
        var languageDic = LanguageObject(this.props.language);
        const product = this.props.product;
        var productTotalChoosen = product.totalChoosen;
        if (productTotalChoosen !== 0 && this.state.inputNumber === 0) {
            this.setState({
                inputNumber: productTotalChoosen,
                totalRowPrice: productTotalChoosen * this.props.product.price
            })
        }

        return (
            <tr>
                <td>
                    <Link 
                        to={{
                            pathname: "/detail",
                            selectedProduct: { product },
                            language: this.props.language
                          }}
                    >{product.name}</Link>
                </td>
                <td>
                    <CounterRowComponent 
                        product={product}
                        handleCounterRowChange={this.handleCounterRowChange}
                    />
                </td>
                <td>{numberWithCommas(product.price, this.props.language)} {languageDic.PriceTag}</td>
                <td>{numberWithCommas(this.state.totalRowPrice, this.props.language)} {languageDic.PriceTag}</td>
            </tr>
        );
    }
}

export default ProductRow;