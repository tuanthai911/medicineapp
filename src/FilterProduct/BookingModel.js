import React from 'react';
import { withRouter } from 'react-router-dom';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { LanguageObject } from '../Helpers/LanguageHelper.js';

class ModalBooking extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            name: '',
            nameValid: false,
            address: '',
            phoneNum: '',
            modal: false
        }
        this.saveButtonHandle = this.saveButtonHandle.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleOnFocus = this.handleOnFocus.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }

    saveButtonHandle(){
        if(!this.state.nameValid || this.state.name === ''){
            this.setState({
                nameValid:false
            }, function () {});
            return;
        }
        this.props.history.push({
            pathname: '/payment',
            state: {
                name: this.state.name,
                products: this.props.products,
                language: this.props.language
            }
        })
    }

    handleInputChange(e){
        var isNameValid = this.state.nameValid;
        var inputId = e.target.id;
        if(e.target.id === 'name'){
            if(e.target.value !== ''){
                isNameValid = true;
            }
            else{
                isNameValid = false;
            }
        }
        this.setState({
            [inputId]: e.target.value,
            nameValid: isNameValid
        }, function () {})
    }

    handleOnFocus(e){
        var isNameValid = false;
        if(e.target.id === 'name'){
            if(e.target.value !== ''){
                isNameValid = true;
            }
            this.setState({
                name: e.target.value,
                nameValid: isNameValid
            }, function () {})
        }
    }

    componentDidMount() {
        this.setState({
            name: '',
            nameValid:true
        });
    }
        
    render(){
        var languageDic = LanguageObject(this.props.language);
        var nameClass = this.state.nameValid ? 'form-control' : 'form-control is-invalid';
        return(
            <div style={{ marginBottom: "5%" }} className="text-right">
                <button type="button" className="btn btn-primary" onClick={this.toggle}>
                <span className="fa fa-shopping-cart"></span>{' '}{languageDic.BookingModel.BookingButton}
                </button>
                <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}>{languageDic.BookingModel.CustomerInfo}</ModalHeader>
                    <ModalBody>
                        <div>
                            <div className="form-group">
                                <label htmlFor="usr">{languageDic.BookingModel.Name}:</label>
                                <input type="text" className={nameClass} id="name" value={this.state.name}
                                    onFocus={this.handleOnFocus}
                                    onChange={this.handleInputChange}
                                />
                                <small className="text-danger" hidden={this.state.nameValid}>
                                    {languageDic.BookingModel.NameError}
                                </small> 
                            </div>
                            <div className="form-group">
                                <label htmlFor="address">{languageDic.BookingModel.Address}:</label>
                                <input type="text" className="form-control" id="address" value={this.state.address} 
                                onChange={this.handleInputChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="phone">{languageDic.BookingModel.Phone}:</label>
                                <input type="number" className="form-control" id="phone" value={this.state.phone} 
                                onChange={this.handleInputChange} />
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <button type="button" className="btn btn-primary" onClick={this.saveButtonHandle}>{languageDic.BookingModel.SaveButton}</button>{' '}
                        <Button color="secondary" onClick={this.toggle}>{languageDic.BookingModel.CloseButton}</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default withRouter(ModalBooking);