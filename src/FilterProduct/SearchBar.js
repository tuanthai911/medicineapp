import React from 'react';
import { LanguageObject } from '../Helpers/LanguageHelper.js';

class SearchBar extends React.Component {
    constructor(props){
      super(props);
      this.handleMedicineInputChange = this.handleMedicineInputChange.bind(this);
    }
  
    handleMedicineInputChange (e){
      this.props.onSearchChange(e.target.value);
    }
  
    render() {
      var languageDic = LanguageObject(this.props.language);
      return (
        <div className="input-group md-form form-sm form-2 pl-0">
          <input
            className="form-control my-0 py-1 red-border"
            type="text"
            placeholder={languageDic.SearchBarInput}
            onChange={this.handleMedicineInputChange}
          />
          <div className="input-group-append">
            <span className="input-group-text red lighten-3" id="basic-text1">
              <i className="fa fa-search text-grey" aria-hidden="true"></i>
            </span>
          </div>
        </div>
      );
    }
}

export default SearchBar;