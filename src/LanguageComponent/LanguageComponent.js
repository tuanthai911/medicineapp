import React from 'react'
import '../css/ToggleButton.css';

class LanguageComponent extends React.Component {
    constructor(props) {
        super(props);
        this.handelToggle = this.handelToggle.bind(this);
    }

    handelToggle() {
        this.props.handelToggle();
    }

    render() {
        return (
            <div>
                <button type="button" class="btn btn-lg btn-secondary btn-toggle" 
                data-toggle="button" aria-pressed="false" autoComplete="off" onClick={this.handelToggle}>
                    <div class="handle"></div>
                </button>
            </div>
        );
    }
}

export default LanguageComponent;