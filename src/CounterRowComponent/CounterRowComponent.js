import React from 'react'

class CounterRowComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputNumber: 0
        }
        this.handleAddButton = this.handleAddButton.bind(this);
        this.handleRemoveButton = this.handleRemoveButton.bind(this);
        this.handleInputValue = this.handleInputValue.bind(this);
    }

    handleAddButton(e) {
        var totalClick = this.state.inputNumber + 1;
        this.setState({
            inputNumber: totalClick
        })
        this.props.product.totalChoosen = totalClick;
        if (this.props.handleCounterRowChange !== undefined) {
            this.props.handleCounterRowChange(totalClick);
        }
    }

    handleRemoveButton(e) {
        var totalClick = this.state.inputNumber - 1;
        if (totalClick < 0) {
            totalClick = 0
        }
        this.setState({
            inputNumber: totalClick
        })
        this.props.product.totalChoosen = totalClick;
        if (this.props.handleCounterRowChange !== undefined) {
            this.props.handleCounterRowChange(totalClick);
        }
    }

    handleInputValue(e) {
        var userInput = e.target.value;
        if (userInput < 0 || userInput === '') {
            userInput = 0;
        }
        this.setState({
            inputNumber: userInput
        })
        this.props.product.totalChoosen = userInput;
        if (this.props.handleCounterRowChange !== undefined) {
            this.props.handleCounterRowChange(userInput);
        }
    }

    render() {
        var productTotalChoosen = this.props.product.totalChoosen;
        if (productTotalChoosen !== 0 && this.state.inputNumber === 0) {
            this.setState({
                inputNumber: productTotalChoosen,
            })
        }
        return (
            <div className="input-group bootstrap-touchspin">
                <span className="input-group-btn" onClick={this.handleRemoveButton}>
                    <button className="btn btn-default bootstrap-touchspin-down" type="button">-</button>
                </span>
                <input id="qty" type="tel"
                    value={this.state.inputNumber}
                    onChange={this.handleInputValue} min="0" max="100"
                    className="form-control" style={{ display: "block" }}
                />
                <span className="input-group-btn" onClick={this.handleAddButton}>
                    <button className="btn btn-default bootstrap-touchspin-up" type="button">+</button>
                </span>
            </div>
        );
    }
}

export default CounterRowComponent;