import React from 'react'
import { numberWithCommas } from '../Helpers/MoneyHelper.js'
import { Link } from 'react-router-dom'
import CounterRowComponent from '../CounterRowComponent/CounterRowComponent.js'
import { LanguageObject } from '../Helpers/LanguageHelper.js'

class ProductDetailPage extends React.Component {
    render() {
        var languageDic = LanguageObject(this.props.location.language);
        var product = this.props.location.selectedProduct.product;
        const WrappedLink = () => {
            return (
                <Link to="/">
                    <button type="button" className="btn btn-info">
                        <span className="fa fa-shopping-cart"></span> {languageDic.DetailPage.BackToShop}
                    </button>
                </Link>
            )
          }

        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm">
                        <img
                            src="https://cdn.tgdd.vn/Products/Images/42/154897/samsung-galaxy-note-9-black-600x600.jpg"
                            className="img-fluid"
                            alt="Responsive image"
                        />
                    </div>
                    <div className="col-sm">
                        <div><h3>{product.name}</h3></div>
                        <div><h4>{numberWithCommas(product.price, this.props.location.language)} {languageDic.PriceTag}</h4></div>
                        <div className="quantity-col1">
                            <p className="quantity-label">{languageDic.DetailPage.Quantity}:</p>
                            <CounterRowComponent product={product}/>
                        </div>
                    </div>
                </div>
                <br/>
                <WrappedLink />
            </div>
        );
    }
}

export default ProductDetailPage;