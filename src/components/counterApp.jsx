import React, { Component } from "react";
import NavBar from "./navbar";
import Counters from "./counters";

class counterApp extends Component {
  state = {
    counters: [
      {
        id: 1,
        value: 0,
      },
      {
        id: 2,
        value: 3,
      },
      {
        id: 3,
        value: 0,
      },
    ],
  };

  render() {
    console.log("App-render");

    return (
      <React.Fragment>
        <NavBar total={this.totalCount()}>
          <main className="container" />
        </NavBar>
        <Counters
          counters={this.state.counters}
          onReset={this.handelReset}
          total={this.totalCount}
          onDelete={this.handleDelete}
          onIncrement={this.handleIncrement}
        />
      </React.Fragment>
    );
  }

  constructor() {
    super();
    console.log("App-construction");
  }
  componentDidMount() {
    // Ajax call
    console.log("componentDidMount");
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log("prevProps", prevState);
    return {};
  }

  handelReset = () => {
    const counters = this.state.counters.map((c) => {
      c.value = 0;
      return c;
    });
    this.setState({
      counters,
    });
  };

  totalCount = () => {
    var total = 0;
    this.state.counters.map((c) => {
      total += c.value;
    });
    return total;
  };

  handleDelete = (counterId) => {
    const counters = this.state.counters.filter((c) => c.id !== counterId);
    this.setState({
      counters,
    });
  };

  handleIncrement = (counter) => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index].value++;
    this.setState({ counters });
  };
}

export default counterApp;
