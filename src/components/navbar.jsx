import React, { Component } from "react";

const NavBar = ({ total }) => {
  console.log("navbar - render");
  return (
    <nav className="navbar navbar-light bg-light">
      <a className="navbar-brand" href="#">
        Total counter = {total}
      </a>
    </nav>
  );
};

export default NavBar;
