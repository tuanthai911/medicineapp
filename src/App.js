import React, { Component } from 'react';
import './css/App.css';
import MedicinePage from './Medicine/MedicinePage.js'
import ProductRow from './FilterProduct/ProductRow';

const PRODUCTS = [
  { name: 'Acigmentin 625mg - augmentin (minh hai)', price: '31000', totalChoosen: 0 },
  { name: 'Acigmentin 1g - augmentin (minh hai)', price: '37000', totalChoosen: 0 },
  { name: 'Acginin 400 (Truong Tho)', price: '44000', totalChoosen: 0 },
  { name: 'Acezin 5 DHG (Theralen DHG )', price: '7500', totalChoosen: 0 },
  { name: 'Acetylcystein 200mg (Nadyphar) chai', price: '34000', totalChoosen: 0 },
  { name: 'Acetylcystein 200 vi - NaDyPhar', price: '45000', totalChoosen: 0 },
  { name: 'Acetylcystein 200 STD (vien)', price: '60000', totalChoosen: 0 },
  { name: 'Acetyl ( vidi ) > h/200v', price: '57000', totalChoosen: 0 },
  { name: 'Acepron 325mg (CL) vien', price: '14000', totalChoosen: 0 },
  { name: 'Acepron 325mg (CL) goi', price: '15000', totalChoosen: 0 },
  { name: 'Acemuc 200 (vien)', price: '61500', totalChoosen: 0 },
  { name: 'Deweton', price: '293000', totalChoosen: 0 },
  { name: 'Detriat', price: '16000', totalChoosen: 0 },
  { name: 'Derma-HG 12g', price: '13000', totalChoosen: 0 },
  { name: 'Embevin 28', price: '77000', totalChoosen: 0 },
  { name: 'Thuoc dau dau', price: '15000', totalChoosen: 0 },
  { name: 'Thuoc giam dau (loai 1)', price: '33000', totalChoosen: 0 },
  { name: 'Thuoc giam dau (loai 2)', price: '34000', totalChoosen: 0 },
  { name: 'Thuoc giam dau (loai 3)', price: '35000', totalChoosen: 0 },
  { name: 'Thuoc giam dau (loai 4)', price: '36000', totalChoosen: 0 },
  { name: 'Thuoc giam dau (loai 5)', price: '37000', totalChoosen: 0 },
  { name: 'Thuoc giam dau (loai 6)', price: '38000', totalChoosen: 0 },
  { name: 'Thuoc giam dau (loai 7)', price: '39000', totalChoosen: 0 },
  { name: 'Thuoc giam dau (loai 8)', price: '30000', totalChoosen: 0 },
  { name: 'Thuoc giam dau (loai 9)', price: '31000', totalChoosen: 0 },
  { name: 'Thuoc giam dau (loai 10)', price: '40000', totalChoosen: 0 },
  { name: 'Thuoc giam dau (loai 11)', price: '50000', totalChoosen: 0 },
  { name: 'Thuoc giam dau (loai 12)', price: '60000', totalChoosen: 0 },
  { name: 'Thuoc giam dau (loai 13)', price: '70000', totalChoosen: 0 },
  { name: 'Thuoc giam dau (loai 14)', price: '80000', totalChoosen: 0 },
  { name: 'Zentomum (Tan Thinh)', price: '50000', totalChoosen: 0 },
  { name: 'Zestril 20mg', price: '41500', totalChoosen: 0 },
  { name: 'Zilgo cuon >', price: '31500', totalChoosen: 0 }
];

class App extends Component {

  constructor(props) {
    super(props);
    this.state = { products: [] };
  }

  async componentDidMount() {
    let result = await fetch('https://localhost:44372/api/Medicine/GetMedicines').then(res => res.json());
    let productItems = [];
    result.forEach(element => {
      productItems.push({
        name: element.name,
        price: element.vnDprice,
        totalChoosen: 0
      })
    });
    this.setState({
      products: productItems
    });
  }

  render() {
    return (
      <MedicinePage products={this.state.products} />
    );
  }
}

export default App;
