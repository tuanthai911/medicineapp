import React from 'react';
import OverView from '../Overview/OverViewComponent.js'
import Navigate from '../Navigate/NavigateComponent.js'
import FilterableProductTable from '../FilterProduct/FilterableProductTable.js'
import LanguageComponent from '../LanguageComponent/LanguageComponent.js'
import { LanguageConst } from '../Helpers/LanguageHelper.js'

class MedicinePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            NumberProductChoosen: 0,
            TotalPrice: 0,
            isChooseState: true,
            language: LanguageConst.Eng
        }
        this.handleProductItemUpdate = this.handleProductItemUpdate.bind(this);
        this.handleProductState = this.handleProductState.bind(this);
        this.handelLanguageToggle = this.handelLanguageToggle.bind(this);
    }

    handleProductState(chooseState) {
        this.setState({
            NumberProductChoosen: this.state.NumberProductChoosen,
            TotalPrice: this.state.TotalPrice,
            isChooseState: chooseState
        });
    }

    handleProductItemUpdate(numberChoose, totalPrice) {
        this.setState({
            NumberProductChoosen: numberChoose,
            TotalPrice: totalPrice,
            isChooseState: this.state.isChooseState
        });
    }

    handelLanguageToggle(){
        var language = this.state.language === LanguageConst.Eng ? LanguageConst.VN : LanguageConst.Eng;
        this.setState({
            language: language
        }, function () {});
    }

    componentDidMount() {
        var totalPrice = 0;
        var totalChoose = 0;
        this.props.products.forEach(
            (product) => {
                if (product.totalChoosen !== 0) {
                    totalChoose++;
                }
                totalPrice += product.totalChoosen * product.price;
            }
        );

        this.setState({
            NumberProductChoosen: totalChoose,
            TotalPrice: totalPrice,
            isChooseState: true,
            language: LanguageConst.Eng
        });
    }

    render() {
        return (
            <div className="container">
                <LanguageComponent 
                    handelToggle={this.handelLanguageToggle}
                />
                <OverView
                    NumberProductChoosen={this.state.NumberProductChoosen}
                    TotalPrice={this.state.TotalPrice}
                    language={this.state.language}
                />
                <Navigate 
                    handleProductState={this.handleProductState} 
                    language={this.state.language}
                />
                <FilterableProductTable
                    products={this.props.products}
                    handleProductItemUpdate={this.handleProductItemUpdate}
                    isChooseState={this.state.isChooseState}
                    language={this.state.language}
                />
            </div>
        );
    }
}

export default MedicinePage;