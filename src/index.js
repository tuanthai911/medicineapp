import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import CounterApp from "./components/counterApp";

import App from './App';
import ProductDetailPage from './DetailPage/ProductDetailPage.js';
import PaymentPage from './PaymentPage/Payment.js'

ReactDOM.render(
    <Router>
        <div>
            <Route exact path="/" component={App} />
            <Route path="/detail" component={ProductDetailPage} />
            <Route path="/payment" component={PaymentPage} />
        </div>
    </Router>,
    //<CounterApp></CounterApp>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
