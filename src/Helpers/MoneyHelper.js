import { LanguageConst } from '../Helpers/LanguageHelper.js';

const numberWithCommas = (x, language) => {
    if(language === LanguageConst.Eng){
        x = (x / LanguageConst.VNDToEng).toFixed(2);;
    }
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export { numberWithCommas }