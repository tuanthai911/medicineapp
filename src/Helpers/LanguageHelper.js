const VNConst = {
    ProductTableFile: {
        ProductName: "Tên sản phẩm",
        ProductPrice: "Giá",
        ProductTotalMoney: "Tổng Tiền"
    },
    PriceTag: "VNĐ",
    SearchBarInput: "Nhập tên thuốc",
    OverViewComponent: {
        Package: "Giỏ hàng",
        Products: "Tổng Sản phẩm",
    },
    NavigateComponent:{
        ChooseProduct: "Chọn hàng",
        Review: "Giỏ hàng"
    },
    BookingModel:{
        BookingButton: "Chốt toa & Đặt hàng",
        CustomerInfo: "Thông tin khách hàng",
        Name: "Họ và tên",
        NameError: "Xin nhập họ và tên",
        Address: "Địa chỉ",
        Phone: "Số điện thoại",
        SaveButton: "Xác nhận",
        CloseButton: "Đóng"
    },
    DetailPage:{
        Quantity: "Số lượng",
        BackToShop: "Quay lại mua hàng"
    },
    PaymentPage:{
        totalChoose: "bạn có {0} hàng trong giỏ hàng",
        BackToStore: "Quay lại mua hàng",
        IncludeVAT: "Đã bao gồm VAT",
        FirstCal: "Tạm tính",
        TotalPay:"Thành tiền",
        Hello: "Xin Chào"
    }
}

const EngConst = {
    ProductTableFile: {
        ProductName: "Product Name",
        ProductPrice: "Price",
        ProductTotalMoney: "Total Money"
    },
    PriceTag: "$",
    SearchBarInput: "Enter medicine name",
    OverViewComponent: {
        Package: "Package",
        Products: "Total Products",
    },
    NavigateComponent:{
        ChooseProduct: "Choose Product",
        Review: "Review"
    },
    BookingModel:{
        BookingButton: "Booking",
        CustomerInfo: "Customer Information",
        Name: "Customer Name",
        NameError: "Please enter your name",
        Address: "Address",
        Phone: "Phone",
        SaveButton: "Book",
        CloseButton: "Close"
    },
    DetailPage:{
        Quantity: "Quantity",
        BackToShop: "Back to shopping"
    },
    PaymentPage:{
        TotalChoose: "You now have {0} quantities",
        BackToStore: "Countiude shopping",
        IncludeVAT: "Include VAT",
        FirstCal: "Sample Payment",
        TotalPay :"Total Payment",
        Hello: "Hello"
    }
}

const LanguageConst = {
    VN: 0,
    Eng: 1,
    VNDToEng: 23000
}

const LanguageObject = (x) => {
    if(x === LanguageConst.Eng){
        return EngConst;
    }
    else{
        return VNConst;
    }
}

export { LanguageConst, LanguageObject }