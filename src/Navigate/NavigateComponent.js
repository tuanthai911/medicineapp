import React from 'react';
import { LanguageObject } from '../Helpers/LanguageHelper.js';

class Navigate extends React.Component{
    constructor(props){
        super(props);
        this.state={
            ChooseClick: true
        }
        this.handleChooseClick = this.handleChooseClick.bind(this);
        this.handleReviewClick = this.handleReviewClick.bind(this);
    }

    handleChooseClick(e){
        this.setState({
            ChooseClick: true
        });
        this.props.handleProductState(true);
    }

    handleReviewClick(e){
        this.setState({
            ChooseClick: false
        });
        this.props.handleProductState(false);
    }

    render(){
        var languageDic = LanguageObject(this.props.language);
        var chooseBtnClassName = this.state.ChooseClick ? 'nav-link active' : 'nav-link ';
        var reviewBtnClassName = this.state.ChooseClick ? 'nav-link' : 'nav-link active';
        
        return(
            <div className="container" style={{marginTop: "2%"}}>
                <ul className="nav nav-pills nav-justified">
                    <li className="nav-item" onClick={this.handleChooseClick}>
                        <a href="#" className={chooseBtnClassName}>{languageDic.NavigateComponent.ChooseProduct}</a>
                    </li>
                    <li className="nav-item" onClick={this.handleReviewClick}>
                        <a href="#" className={reviewBtnClassName}>{languageDic.NavigateComponent.Review}</a>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Navigate;